import React, { Component } from "react";
import { render } from "react-dom";
import Nav  from './Nav.js';
import Main from "./Main.js";
import Footer from './Footer.js'
import { BrowserRouter as Router } from "react-router-dom";

class App extends Component {


  render() {
    return (
      <Router>
        <Nav />
        <Main />
        <Footer />
      </Router>
    );
  }
}

export default App;
