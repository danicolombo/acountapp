import React, { Component } from "react";
import { render } from "react-dom";
import { styles } from './styles.js';
import { Link } from "react-router-dom";

class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: moment().format("MMMM DD, YYYY"),
    };
  }
  render (props) {

    return(
      <nav  className="navbar" style={styles.footer}>
        <div className="container">
        <Link className="navbar-brand" to="/" style={styles.links}>Accounting App</Link>
        <p style={styles.date}>{this.state.date}</p>
        </div>
      </nav>
    )
  }
}
export default Nav;
