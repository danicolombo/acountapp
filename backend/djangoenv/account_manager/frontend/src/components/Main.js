import React, { Component } from "react";
import {  Route } from "react-router-dom";
import Transactions from "./Transactions";
import Form from "./Form";

class Main extends Component {
  render(){
    return(
      <div>
      <Route exact path="/" component={Transactions} />
      <Route exact path="/form" component={Form} />
      </div>
    )
  }
}
export default Main;
