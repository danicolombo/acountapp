from django.db import models

# Create your models here.

class Transaction(models.Model):
    TRANSACTION_CHOICES = [
        ('DB', 'Debit'),
        ('CR', 'Credit'),
    ]
    title = models.CharField(max_length=200)
    completed = models.BooleanField(default=False, blank=True, null=True)
    transaction_type = models.CharField(max_length=10, choices=TRANSACTION_CHOICES)
    amount = models.DecimalField(decimal_places=2, max_digits=10000)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.title
