# Accounting App 

Este proyecto fue realizado para un desafío de una empresa de software. 
Consiste en una webApp en la que se pueden cargar transacciones monetarias con su descripción, tipo (débito o crédito) y monto.


## Comenzando

Clona el repositorio desde gitlab.com a tu espacio de trabajo:

$ git clone https://gitlab.com/danicolombo/acountapp.git

Navega dentro de la carpeta del repositorio y crea un ambiente virtual dentro de la carpeta del proyecto y actívalo:
$ virtualenv .
$ source bin/activate

Instala sus requerimientos:
$ pip install -r requirements.txt

En la carpeta principal ejecuta las migraciones:
$ python manage.py makemigrations
$ python manage.py migrate

Inicializa el servidor de Django
$ python manage.py runserver

Navega dentro de la carpeta frontend, instala los módulos de npm y inicializa el webpack:
$ npm install
$ npm run dev


## Construído con

* [Django](https://www.djangoproject.com/) 
* [Django Rest Framework](https://www.django-rest-framework.org/) 
* [React](https://es.reactjs.org/) 
* [Bootstrap](https://getbootstrap.com/) 


## Autor

* **Daniela Colombo** - *más trabajos* - [Portfolio](https://procnedc.github.io/resume)


## Licencia

Este proyecto está bajo una licencia MIT - ver la [licencia](https://opensource.org/licenses/MIT) para mas detalles.
 
